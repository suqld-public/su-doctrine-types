<?php

namespace SUA\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

/**
 * Doctrine <2.6 has no date or time interval types. Replicate that functionality.
 */
class TimeIntervalType extends Type
{
    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform)
    {
        return 'int';
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        if ($value === null || $value instanceof TimeInterval) {
            return $value;
        }

        return new TimeInterval($value);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        /** @var TimeInterval $value */

        return ($value !== null) ? $value->getTotalSeconds() : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'timeinterval';
    }

    /**
     * {@inheritdoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
