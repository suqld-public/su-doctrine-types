<?php

namespace SUA\Types;

use JsonSerializable;

class TimeInterval implements JsonSerializable
{
    /** @var int */
    private $totalSeconds;

    /* Storage of Hours, Minutes and Seconds is for convenience, never used for any calculations */
    /** @var int */
    private $hours;

    /** @var int */
    private $minutes;

    /** @var int */
    private $seconds;

    const HOURS_PER_WORKDAY = 7.6;

    /**
     * @param int|float $seconds The number of seconds for the interval. Although we only deal with integer seconds, we
     *                           accept floats as we often do floating point math prior to passing in the number of
     *                           seconds into the constructor.
     */
    public function __construct(float $seconds = 0)
    {
        $this->totalSeconds = round($seconds);
        $this->normalise();

        return $this;
    }

    /**
     * @param TimeInterval $timeInterval
     *
     * @return $this
     */
    public function add(TimeInterval $timeInterval)
    {
        $this->totalSeconds += $timeInterval->getTotalSeconds();
        $this->normalise();

        return $this;
    }

    /**
     * @param TimeInterval $timeInterval
     *
     * @return $this
     */
    public function sub(TimeInterval $timeInterval)
    {
        $this->totalSeconds -= $timeInterval->getTotalSeconds();
        $this->normalise();

        return $this;
    }

    /**
     * @return string
     */
    public function formatHoursMins()
    {
        return sprintf('%d:%02d', $this->hours, abs($this->minutes));
    }

    /**
     * @return float
     */
    public function getHoursDecimal()
    {
        return $this->hours + $this->minutes / 60 + $this->seconds / 60 / 60;
    }

    /**
     * @return float
     */
    public function getDays()
    {
        // Rounding fixes floating point issues
        return round($this->getHoursDecimal() / self::HOURS_PER_WORKDAY, 4);
    }

    /**
     * @return string
     */
    public function getDaysString()
    {
        $comparison = round($this->getDays() * 4, 4);
        if ((int) $comparison != $comparison) {
            $string = '<small>approx. </small>';
            $precision = 1;
        } else {
            $string = '';
            $precision = 2;
        }

        return $string . round($this->getDays(), $precision) . ' day' . ($this->getDays() == 1 ? '' : 's');
    }

    /**
     * @return bool
     */
    public function shouldShowDays()
    {
        return (int) $this->getDays() == $this->getDays();
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return ($this->hours == 0) && ($this->minutes == 0) && ($this->seconds == 0);
    }

    /**
     * Format in easy to read compact format, but using HTML to make the h and m small. e.g. 4h 25m
     *
     * @param bool $includeDays
     * @param bool $alwaysShowDays
     *
     * @return string
     */
    public function formatHtml(bool $includeDays = false, bool $alwaysShowDays = false)
    {
        $string = $this->hours . '<small>h</small> ' . abs($this->minutes) . '<small>m</small>';
        if ($includeDays && ($this->shouldShowDays() || $alwaysShowDays)) {
            $string .= ' (' . $this->getDaysString() . ')';
        }

        return $string;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->formatHtml();
    }

    /**
     * In general, we are storing as seconds in the database, but if we need a MySQL formatted TimeString,
     * this is available
     *
     * @return string
     */
    public function getMySQLTimeString()
    {
        return sprintf('%d:%02d:%02d', $this->hours, abs($this->minutes), abs($this->seconds));
    }

    /**
     * @return int
     */
    public function getTotalSeconds()
    {
        return $this->totalSeconds;
    }

    /**
     * @param string $time A MySQL time string (hh:mm:ss)
     *
     * @return TimeInterval
     */
    public static function createFromTimestamp(string $time = '00:00:00')
    {
        $isNegative = (substr($time, 0, 1) === '-');
        [$hours, $minutes, $seconds] = explode(':', ltrim($time, '-'));
        $totalSeconds = $hours * 60 * 60;
        $totalSeconds += $minutes * 60;
        $totalSeconds += $seconds;
        if ($isNegative) {
            $totalSeconds *= -1;
        }

        return new TimeInterval($totalSeconds);
    }

    /**
     * Specify data which should be serialized to JSON
     *
     * @see  http://php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed data which can be serialized by <b>json_encode</b>,
     *               which is a value of any type other than a resource
     *
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'days'         => $this->getDays(),
            'daysString'   => $this->getDaysString(),
            'hours'        => $this->getHoursDecimal(),
            'seconds'      => $this->getTotalSeconds(),
            'hoursMinutes' => $this->formatHoursMins(),
            'html'         => $this->formatHtml(),
        ];
    }

    /**
     * Take the totalSeconds and normalise it out into Hours, Minutes, Seconds
     */
    private function normalise()
    {
        /*
         * Move >=60 seconds to minutes
         */
        $this->minutes = intdiv($this->totalSeconds, 60);
        $this->seconds = $this->totalSeconds % 60;

        /*
         * Move >=60 minutes to hours.
         */
        $this->hours = intdiv($this->minutes, 60);
        $this->minutes = $this->minutes % 60;
    }
}
