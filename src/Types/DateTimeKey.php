<?php

namespace SUA\Types;

use DateTime;

/**
 * Required so symfony can convert DateTime to string when used in primary keys.
 */
class DateTimeKey extends DateTime
{
    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->format('U');
    }
}
