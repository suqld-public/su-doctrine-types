<?php

namespace SUA\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\DateType;

/**
 * Required so symfony can convert DateTime to string when used in primary keys.
 */
class DateKeyType extends DateType
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'datekey';
    }

    /**
     * When used in composite keys, datekey is converted to a timestring before getting here
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return is_numeric($value)
            ? (new DateKey())->setTimestamp($value)->format($platform->getDateFormatString())
            : parent::convertToDatabaseValue($value, $platform);
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        $dateTime = parent::convertToPHPValue($value, $platform);

        if (!$dateTime) {
            return $dateTime;
        }

        return (new DateKey('@' . $dateTime->format('U')))->setTimezone($dateTime->getTimezone());
    }

    /**
     * {@inheritdoc}
     */
    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
