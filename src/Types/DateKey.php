<?php

namespace SUA\Types;

use DateTime;

/**
 * Required so symfony can convert DateTime to string when used in primary keys.
 */
class DateKey extends DateTime
{
    public function __toString()
    {
        return (string) $this->format('U');
    }
}
